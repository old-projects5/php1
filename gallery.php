<style type="text/css">

img
{
 width:auto;
 box-shadow:0px 0px 20px #cecece;
 -moz-transform: scale(0.7);
 -moz-transition-duration: 0.6s; 
 -webkit-transition-duration: 0.6s;
 -webkit-transform: scale(0.7);
 -ms-transform: scale(0.7);
 -ms-transition-duration: 0.6s;
}
img:hover
{
  box-shadow: 20px 20px 20px #dcdcdc;
 -moz-transform: scale(0.8);
 -moz-transition-duration: 0.6s;
 -webkit-transition-duration: 0.6s;
 -webkit-transform: scale(0.8);
 -ms-transform: scale(0.8);
 -ms-transition-duration: 0.6s;
 outline:1px solid #FFF;
}
</style>

<?php
// integer starts at 0 before counting
$i = 0; 
$dir = 'gallery/';
if ($handle = opendir($dir)) {
	while (($file = readdir($handle)) !== false){
		if (!in_array($file, array('.', '..')) && !is_dir($dir.$file)) 
			$i++;
	}
}

for ($j=1; $j<$i+1; $j++)
echo "<a href='galleryone.php?foto=$j'><img src='$dir$j.jpg?timestamp=".time()."'  height='250' /></a>";?>
